import {defineConfig, squooshImageService} from 'astro/config';
import mdx from '@astrojs/mdx';
import vue from '@astrojs/vue';
import react from '@astrojs/react'
import sitemap from '@astrojs/sitemap';
import tailwind from '@astrojs/tailwind'; //

// 中间件模块
import proxyMiddleware from './src/middleware/http-proxy-md/index.mjs';
// https://astro.build/config

export default defineConfig({
    base: '/',
    image: {
        service: squooshImageService(),
    },
    site: 'https://example.com',
    integrations: [
        tailwind(), vue(),react(), mdx(), sitemap(),
        proxyMiddleware('/api', {
            target: 'https://work_test.3d66.com/',
            changeOrigin: true,
        })
    ],
    server: {
        port: 9999,
        host: true,
    },

});
