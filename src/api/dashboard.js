import {service} from './http.js';


export const getList = (params) => {
    return service({
        method: 'get',
        url: '/api/customer/list',
        params
    })
}