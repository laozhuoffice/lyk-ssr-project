import axios from 'axios'
const baseUrl = ''

export const service = axios.create({
    baseURL: baseUrl,
    timeout: 30000,
    headers: {},
    responseType: 'json',
    withCredentials: true, // 是否允许带cookie这些
})

const successCode = [200, 0]
service.interceptors.response.use(response => {
    if (successCode.includes(response.status)) {
        return Promise.resolve(response.data)
    }
})